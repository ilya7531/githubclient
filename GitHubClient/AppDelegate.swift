//
//  AppDelegate.swift
//  GitHubClient
//
//  Created by Katya on 6/14/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

}

