//
//  DataService.swift
//  GitHubClient
//
//  Created by Ilya on 7/23/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import Foundation
import Alamofire

final class DataService {
    
    //MARK: Singleton
    
    static let shared = DataService()
    private init() {}
    
    // MARK: - Public Methods
    
    func getUsers(url: String ,completion: @escaping (UserModelResponse) -> Void) {
        guard let url = URL(string: url) else { return }
        
        NetworkingService.shared.getData(fromURL: url) { (json) in
            do {
                if let response = try UserModelResponse(json: json) {
                   completion(response)
                }
            } catch {
                print("ERROR: Receiving UserModelResponse failed")
            }
        }
    }
    
    func getUserDetails(url: String ,completion: @escaping (UserDetailsModelResponse) -> Void) {
        guard let url = URL(string: url) else { return }
        
        NetworkingService.shared.getData(fromURL: url) { (json) in
            do {
                if let response = try UserDetailsModelResponse(json: json) {
                    completion(response)
                }
            } catch {
                print("ERROR: Receiving UserDetailsModelResponse failed")
            }
        }
    }
    
}
