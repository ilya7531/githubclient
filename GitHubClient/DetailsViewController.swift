//
//  DetailsViewController.swift
//  GitHubClient
//
//  Created by Katya on 6/15/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import UIKit
import Alamofire

class DetailsViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var followersLabel: UILabel!
    @IBOutlet private weak var companyLabel: UILabel!
    @IBOutlet private weak var registrationDateLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var followingLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    
    // MARK: - Public Properties
    
    var login = String()
    
    // MARK: - Private Properties
    
    private var outUserDetailsModel = UserDetailsModel()
    private let baseUsersURL = "https://api.github.com/users/"
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = login
        loadUserDetails()
    }
    
    // MARK: - Private Methods
    
    private func loadUserDetails() {
        DataService.shared.getUserDetails(url: baseUsersURL + login) { (response) in
            let userDetailsModel = response.userDetailsModel
            self.outUserDetailsModel = userDetailsModel
            if self.outUserDetailsModel != nil {
                self.displayUserDetails(self.outUserDetailsModel!)
            }
        }
    }
    
    private func displayUserDetails(_ model: UserDetailsModel) {
        followersLabel.text = "Followers " + String(model.followers)
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        let tmpDate = dateFormat.string(from: model.registrationDate)
        registrationDateLabel.text = "Date: " + tmpDate
        companyLabel.text = "Company: " + model.company
        emailLabel.text = "Email: " + model.email
        followingLabel.text = "Following " + String(model.following)
        nameLabel.text = model.login
        NetworkingService.shared.getImage(fromURL: model.avatarURL) { (avatar) in
            self.setImage(avatar: avatar)
        }
    }
    
    private func setImage(avatar: UIImage) {
        self.avatarImageView.image = avatar
    }
    
}
