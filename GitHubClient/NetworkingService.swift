//
//  NetworkingService.swift
//  GitHubClient
//
//  Created by Ilya on 7/23/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import Foundation
import Alamofire

final class NetworkingService {
    
    //MARK: Singleton
    
    private init() {}
    static let shared = NetworkingService()
    
    // MARK: - Public Methods
    
    func getData(fromURL url: URL, completion: @escaping (Data)->Void) {
        Alamofire.request(url, method: .get).validate().responseJSON { response in
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    print("Successful request")
                default:
                    print("Error with response status: \(status)")
                }
            }
            if response.result.value != nil {
                guard let responseData = response.data else {return}
                DispatchQueue.main.async {
                    completion(responseData)
                }
            }
        }
    }
   
    func getImage(fromURL url: String, completion: @escaping (UIImage)->Void) {
        if let imageURL = URL(string: url) {
            DispatchQueue.global().async {
                let imageData = try? Data(contentsOf: imageURL)
                if let data = imageData{
                    let image = UIImage(data: data)
                    DispatchQueue.main.async{
                        completion(image ?? UIImage())
                    }
                }
            }
        }
    }
    
}
