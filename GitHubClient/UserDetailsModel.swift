//
//  UserDetailsModel.swift
//  GitHubClient
//
//  Created by Katya on 6/15/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import Foundation

struct UserDetailsModel {
    
    // MARK: - Public Properties
    
    var login: String
    var avatarURL: String
    var email: String
    var company: String
    var following: Int
    var followers: Int
    var registrationDate : Date
    
    // MARK: - Initializers
    
    init?() {
        self.login = ""
        self.avatarURL = ""
        self.email = ""
        self.company = ""
        self.following = 0
        self.followers = 0
        self.registrationDate = Date()
    }
    
}

// MARK: - Protocol Decodable

extension UserDetailsModel: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case login
        case avatarURL = "avatar_url"
        case email
        case company
        case following
        case followers
        case registrationDate = "created_at"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.login = try container.decode(String.self, forKey: .login)
        self.avatarURL = try container.decode(String.self, forKey: .avatarURL)
        self.email = try container.decodeIfPresent(String.self, forKey: .email) ?? ""
        self.company = try container.decodeIfPresent(String.self, forKey: .company) ?? ""
        self.following = try container.decode(Int.self, forKey: .following)
        self.followers = try container.decode(Int.self, forKey: .followers)
        self.registrationDate = try container.decode(Date.self, forKey: .registrationDate)
    }
    
}
