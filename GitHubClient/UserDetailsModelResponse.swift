//
//  UserDetailsModelResponse.swift
//  GitHubClient
//
//  Created by Ilya on 7/23/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import Foundation

struct UserDetailsModelResponse {
    
    // MARK: - Public Properties
    
    var userDetailsModel = UserDetailsModel()
    
    // MARK: - Initializers
    
    init?(json: Data) throws {
        do {
            let decoder = JSONDecoder()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            decoder.dateDecodingStrategy = .formatted(formatter)
            let userDetailsModelTmp = try decoder.decode(UserDetailsModel.self, from: json)
            self.userDetailsModel = userDetailsModelTmp
        } catch {
            print("ERROR: Decoding users details failed")
        }
    }
    
}
