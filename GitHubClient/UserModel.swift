//
//  UserModel.swift
//  GitHubClient
//
//  Created by Katya on 6/15/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import Foundation

struct UserModel {
    
    // MARK: - Public Properties
    
    var login: String
    var id: Int
    var avatarURL: String
}

// MARK: - Protocol Decodable

extension UserModel: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case login
        case avatarURL = "avatar_url"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.login = try container.decode(String.self, forKey: .login)
        self.avatarURL = try container.decode(String.self, forKey: .avatarURL)
    }
    
}


