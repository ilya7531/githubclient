//
//  UserModelResponse.swift
//  GitHubClient
//
//  Created by Ilya on 7/23/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import Foundation

struct UserModelResponse {
    
    // MARK: - Public Properties
    
    var userModelArray: [UserModel]
    
    // MARK: - Initializers
    
    init?(json: Data) throws {
        var userModelArrayTmp = [UserModel]()
        do {
            let usersModels = try JSONDecoder().decode([UserModel].self, from: json)
            for userModel in usersModels {
                userModelArrayTmp.append(userModel)
            }
        } catch {
            print("ERROR: Decoding users failed")
        }
        self.userModelArray = userModelArrayTmp
    }
    
}
