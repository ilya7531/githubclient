//
//  UserTableViewCell.swift
//  GitHubClient
//
//  Created by Katya on 6/15/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet private weak var loginLabel: UILabel!
    @IBOutlet private weak var idLabel: UILabel!
    @IBOutlet private weak var avatarImageView: UIImageView!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Public Methods
    
    func setLabel(_ model: UserModel) {
        loginLabel.text = model.login
        idLabel.text = String(model.id)
    }
    
    func setImage(avatar: UIImage) {
        avatarImageView.image = avatar
    }
    
}
