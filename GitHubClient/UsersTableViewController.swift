//
//  UsersTableViewController.swift
//  GitHubClient
//
//  Created by Katya on 6/15/19.
//  Copyright © 2019 Ilya Pritula. All rights reserved.
//

import UIKit
import Alamofire

class UsersTableViewController: UITableViewController {
   
    // MARK: - IBOutlets
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Private Properties
    
    private let baseURL = "https://api.github.com/users"
    private var usersCounter = 0
    private var outUserModelArray = [UserModel]()
    private var usersDataLoader: (_ response : UserModelResponse)->() = { _ in }
        
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUsersDataLoader()
        getUsers()
        self.tableView.refreshControl = self.refresher
    }
    
    // MARK: - Private Methods
    
    private func initUsersDataLoader() {
        usersDataLoader = { response in
            let usersFromResponseArray = response.userModelArray
            self.outUserModelArray += usersFromResponseArray
            self.tableView.reloadData()
            self.stopAnimationAfterDataLoading()
        }
    }
    
    private func stopAnimationAfterDataLoading() {
        self.refresher.endRefreshing()
        self.activityIndicator.stopAnimating()
    }
    
    private func getUsers() {
        DataService.shared.getUsers(url: baseURL, completion: usersDataLoader)
    }
    
    // MARK: - UIRefreshControl
    
    lazy var refresher : UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadTableView), for: .valueChanged)
        return refreshControl
    }()
    
    @objc func reloadTableView() {
        self.outUserModelArray.removeAll()
        usersCounter = 0
        DataService.shared.getUsers(url: baseURL, completion: usersDataLoader)
    }

    // MARK: - Table View Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return outUserModelArray.count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as? UserTableViewCell {
            if outUserModelArray.count > indexPath.row {
                let userCell = outUserModelArray[indexPath.row]
                cell.setLabel(userCell)
                NetworkingService.shared.getImage(fromURL: userCell.avatarURL) { (avatar) in
                    cell.setImage(avatar: avatar)
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    // MARK: - Pagination
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == outUserModelArray.count - 1 {
            self.activityIndicator.startAnimating()
            self.perform(#selector(expandTable), with: nil, afterDelay: 1.0)
        }
    }
    
    @objc func expandTable() {
        usersCounter = outUserModelArray.count > 0 ? outUserModelArray[outUserModelArray.count-1].id : 0
        DataService.shared.getUsers(url: baseURL+"?since=\(usersCounter)", completion: usersDataLoader)
    }

    // MARK: - Navigation
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsViewController = storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController
        if detailsViewController != nil {
            detailsViewController?.login = outUserModelArray[indexPath.row].login
            self.navigationController?.pushViewController(detailsViewController!, animated: true)
        }
    }
    
}
